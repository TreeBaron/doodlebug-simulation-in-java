import java.util.ArrayList;

public class World
{
	
	//static list of organisms within the world
	public static ArrayList<Organism> Creatures = new ArrayList<Organism>();
	
	//how long has the world been going?
	int WorldTime = 0;
	
	static int WorldSize = 0;
	static char[][] WorldMap = null;
	
	//constructor...
	World(int size)
	{
		GenerateMap(size);
	}
	
	World(int size,int DoodleBugs,int Ants)
	{
		GenerateMap(size);
		
		//populate map with doodlebugs
		for(int i = 0; i < DoodleBugs; i++)
		{
			Creatures.add(new DoodleBug());
		}
		
		//populate map with ants
		for(int i = 0; i < Ants; i++)
		{
			Creatures.add(new Ant());
		}
		
	}
	
	public void GenerateMap(int size)
	{
		WorldSize = size;
		WorldMap = new char[size][size];
		
		//populate with default char of '-'
		for(int y = 0; y < size; y++)
		{
			for(int x = 0; x < size; x++)
			{
				WorldMap[y][x] = '-';
			}
		}
	}

	public void Update()
	{
		//update all organisms within the world
		
		//clear map of dead organism
		ClearDeadOrganisms();
		
		//update all creatures on map, move, eat, breed etc
		UpdateAllCreatures();
		
		//clear all map chars to '-'
		ClearMap();
		
		//update creature locations on map
		UpdateCreatureLocationsOnMap();
		
	}

	
	private void ClearMap()
	{
		//refresh look of map
				for(int y = 0; y < WorldSize; y++)
				{
					for(int x = 0; x < WorldSize; x++)
					{
						WorldMap[y][x] = '-';
					}
				}
	}
	
	private void UpdateCreatureLocationsOnMap()
	{
		//correct map locations now that creatures have done their things
		for(int i = 0; i < Creatures.size();i++)
		{
			WorldMap[Creatures.get(i).getY()][Creatures.get(i).getX()] = Creatures.get(i).Name.charAt(0);
		}
	}
	
	private void UpdateAllCreatures()
	{
		
			//update all creatures based on update priority
			for(int i = 0; i < Creatures.size(); i++)
			{
				if(Creatures.get(i).TurnPriority == 1)
				{
					//update organism
					Creatures.get(i).OrganismUpdate();
					//update creature
					Creatures.get(i).Update();
				}
			}
		
			//update all creatures based on update priority
			for(int i = 0; i < Creatures.size(); i++)
			{
				if(Creatures.get(i).TurnPriority == 2)
				{
					//update organism
					Creatures.get(i).OrganismUpdate();
					//update creature
					Creatures.get(i).Update();
				}
			}
		
	}
	
	private void ClearDeadOrganisms()
	{
		//remove dead organisms...
		for(int i = 0; i < Creatures.size(); i++)
		{
			if (Creatures.get(i).Alive == false)
			{
				//remove dead creature reference
				Creatures.remove(i);
			}
		}
		
	}
	
	//check if a location is clear of any creature...
	public static boolean CheckClear(int x, int y)
	{
		//check if index is within bounds of array size...
		if(x < 0 || y < 0)
		{
			return false;
		}
		
		//outside array!
		if(x > WorldSize-1 || y > WorldSize-1)
		{
			return false;
		}
		
		//check all creatures to see if they occupy location
		for(int i = 0; i < Creatures.size(); i++)
		{
			if(Creatures.get(i).getX() == x && Creatures.get(i).getY() == y)
			{
				return false;
			}
		}
		
		return true;
	}
	
	public static Organism GetCreatureAt(int x, int y)
	{
		//find creature at coordinate and kill
		for(int i = 0; i < Creatures.size(); i++)
		{
			if(Creatures.get(i).getX() == x && Creatures.get(i).getY() == y)
			{
				return Creatures.get(i);
			}
		}
		
		//cannot find creature, then return null
		return null;
	}
	
	//****************************************************************
	//Getters and Setters...

	public int getWorldTime()
	{
		return WorldTime;
	}

	public void setWorldTime(int worldTime)
	{
		WorldTime = worldTime;
	}

	public static int getWorldSize()
	{
		return WorldSize;
	}

	public static void setWorldSize(int worldSize)
	{
		WorldSize = worldSize;
	}

	public static char[][] getWorldMap()
	{
		return WorldMap;
	}

	public static void setWorldMap(char[][] worldMap)
	{
		WorldMap = worldMap;
	}
	
}
