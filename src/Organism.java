
public class Organism
{

	//constructor for an organism random location
	Organism(String name,int breedtime,int turnpriority)
	{
		//validate string
		if (name.equals(""))
		{
			name = "U";
		}
		
		Name = name;
		BreedTime = breedtime;
		TurnPriority = turnpriority;

		//set min and max of world...
		int min = 0;
		int max = World.WorldSize -2;
		//declare var to hold random
		int rando = 0;
		
		//set initial x and y coordinates
		rando = min + (int)(Math.random() * max);
		X = rando;
		rando = min + (int)(Math.random() * max);
		Y = rando;
		
		//make sure that there's no creature overlap...
		for(int i = 0; i < World.Creatures.size(); i++)
		{
			//Inefficient for sure...
			while(X == World.Creatures.get(i).getX() && Y == World.Creatures.get(i).getY())
			{
				rando = min + (int)(Math.random() * max);
				X = rando;
				rando = min + (int)(Math.random() * max);
				Y = rando;
				//reset to beginning of list
				i = 0;
			}
			
		}
		
		//set world map...
		World.WorldMap[Y][X] = name.charAt(0);
		
	}
	
	//constructor for an organism assigned location
	Organism(String name,int breedtime,int turnpriority,int x,int y)
	{
		//validate string
		if (name.equals(""))
		{
			name = "U";
		}
		
		Name = name;
		BreedTime = breedtime;
		TurnPriority = turnpriority;

		X = x;
		Y = y;
		
		//add organism to list
		World.Creatures.add(this);
		
	}
	
	public void SetLocation(int x, int y)
	{
		X = x;
		Y = y;
	}
	
	
	//variables for each organism
	String Name = "Default Name";
	
	//location of organism
	int X = 0;
	int Y = 0;

	//if the creature is alive
	boolean Alive = true;
	
	//how long organism has been alive
	int AliveTime = 0;
	
	//How many steps it takes before this animal can breed
	int BreedTime = 3; //one step by default
	
	//store the last time this creature ate something
	int TimeSinceLastMeal = 0;
	
	//how long since last breeding
	int TimeSinceLastBreeding = 0;
	
	//Turn priority the higher the number the more priority this organism has
	int TurnPriority = 0;
	
	public void Update()
	{
		//this is a method to be overridden by the organism specified
	}
	
	public void OrganismUpdate()
	{
		AliveTime++;
	}

	
	//*****************************************************************************
	//generated getters and setters
	public int getX() 
	{
		return X;
	}

	public void setX(int x) 
	{
		X = x;
	}

	public int getY() 
	{
		return Y;
	}

	public void setY(int y) 
	{
		Y = y;
	}
	
}
