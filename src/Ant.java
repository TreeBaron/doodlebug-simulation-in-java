import javax.swing.JOptionPane;

public class Ant extends Organism 
{
		//public static int AntCount = 0;
	
		//constructor
		Ant()
		{
			//breedtime is 3, turn priority is 2, name is "Ant"
			super("Ant", 3, 2);
			
		}

		//constructor with x and y coordinates...
		Ant(int x, int y)
		{
			//breedtime is 3, turn priority is 2, name is "Ant"
			super("Ant", 3, 2,x,y);
			
		}
		
		//update for ant
		public void Update()
		{
			//check if ant can move...
			Move();
			
			//check if ant can breed...
			Breed();
		}
		
		//decide where to move...
		public void Move()
		{
			//generate "random" movement
			int min = 0;
			int max = 3;
			int moveXorY = 1 + (int)(Math.random() * 2);
			int rando = min + (int)(Math.random() * max)-1;
			
			//JOptionPane.showMessageDialog(null,"Move X or Y ="+moveXorY+"");
			
			//prevent 0 from being generated
			while(rando == 0)
			{
				rando = min + (int)(Math.random() * max)-1;
			}
			
			//prevent diagonal movement!
			if (moveXorY == 1)
			{
				//check clear above
				if(World.CheckClear((X+rando),Y))
				{
					X = X+rando;
					return;
				}
			}
			else
			{
				//check clear above
				if(World.CheckClear(X,(Y+rando)))
				{
					Y = Y + rando;
					return;
				}
			}
		
		}
		
		//check if it can breed
		public void Breed()
		{
			//increase time since last breeding
			TimeSinceLastBreeding++;
			
			if(TimeSinceLastBreeding > BreedTime)
			{
				//JOptionPane.showMessageDialog(null,"Breeding ant. Ant Count: "+AntCount);
				
				//reset breedtime
				TimeSinceLastBreeding = 0;
				
				//check clear above
				if(World.CheckClear(X,(Y+1)))
				{
					Ant a = new Ant(X,(Y+1));
					return;
				}
				//check clearbelow...
				if(World.CheckClear(X, (Y-1)))
				{
					Ant a = new Ant(X,(Y-1));
					return;
				}
				//check clear right
				if(World.CheckClear((X+1), Y))
				{
					Ant a = new Ant((X+1),Y);
					return;
				}
				//check clear left
				if(World.CheckClear((X-1), Y))
				{
					Ant a = new Ant((X-1),Y);
					return;
				}
			}
		}

}
