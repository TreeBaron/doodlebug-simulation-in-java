import javax.swing.JOptionPane;

public class DoodleBug extends Organism
{
	//public static int DoodleBugCount = 0;

	//constructor
	DoodleBug()
	{
		//breedtime is 8, turn priority is 1, name is "DoodleBug"
		super("DoodleBug", 8, 1);
		
	}
	
	//constructor with coordinates
	DoodleBug(int x, int y)
	{
		//breedtime is 8, turn priority is 1, name is "DoodleBug"
		super("DoodleBug", 8, 1,x,y);
		
	}

	//update for doodlebug
	public void Update()
	{
		TimeSinceLastMeal++;
		
		//if bug can't find anything to eat
		if (!Eat())
		{
			//then it moves aimlessly
			Move();
		}
			
		//breed the bug
		Breed();
		
		//does the bug starve?
		if(TimeSinceLastMeal > 4)
		{
			//doodlebug dies :(
			this.Alive = false;
		}
	}
	
	//decide where to move...
	public boolean Eat()
	{
		//basically the doodlebug will try to move towards an ant within range and eat it

		//check clear above
		if(!World.CheckClear(X,(Y+1)))
		{
			if (AntAt(X,(Y+1)))
			{
				//reset food clock
				TimeSinceLastMeal = 0;
				
				//eat creature
				World.GetCreatureAt(X,(Y+1)).Alive = false;
				
				//move to position
				Y = Y+1;

				
				return true;
			}
		
		}
		//check clearbelow...
		if(!World.CheckClear(X, (Y-1)))
		{
			if (AntAt(X,(Y-1)))
			{
				//reset food clock
				TimeSinceLastMeal = 0;
				
				//eat creature
				World.GetCreatureAt(X,(Y-1)).Alive = false;
				
				//move to position
				Y = Y-1;
			
				
				return true;
			}
		}
		//check clear right
		if(!World.CheckClear((X+1), Y))
		{
			if (AntAt((X+1),Y))
			{
				//reset food clock
				TimeSinceLastMeal = 0;
				
				//eat creature
				World.GetCreatureAt((X+1),Y).Alive = false;
				
				//move to position
				X = X+1;
				
				return true;
			}
		}
		//check clear left
		if(!World.CheckClear((X-1), Y))
		{
			if (AntAt((X-1),Y))
			{
				//reset food clock
				TimeSinceLastMeal = 0;
				
				//eat creature
				World.GetCreatureAt((X-1),Y).Alive = false;
				
				//move to position
				X = X-1;
			
				
				return true;
			}
		}
		
		//can't find something to eat! D:
		return false;
		
	}
	
	//move somewhere
	public void Move()
	{
		//generate "random" movement
		int min = 0;
		int max = 3;
		int moveXorY = 1 + (int)(Math.random() * 2);
		int rando = min + (int)(Math.random() * max)-1;
		
		//JOptionPane.showMessageDialog(null,"Move X or Y ="+moveXorY+"");
		
		//prevent 0 from being generated
		while(rando == 0)
		{
			rando = min + (int)(Math.random() * max)-1;
		}
		
		//prevent diagonal movement!
		if (moveXorY == 1)
		{
			//check clear above
			if(World.CheckClear((X+rando),Y))
			{
				X = X+rando;
				return;
			}
		}
		else
		{
			//check clear above
			if(World.CheckClear(X,(Y+rando)))
			{
				Y = Y + rando;
				return;
			}
		}
		
	}
	
	//check if it can breed
	public void Breed()
	{
		//increase time since last breeding
		TimeSinceLastBreeding++;
		
		if(TimeSinceLastBreeding >= BreedTime)
		{
			//check clear above
			if(World.CheckClear(X,(Y+1)))
			{
				//JOptionPane.showMessageDialog(null,"Doodle just bred.");
				DoodleBug DB = new DoodleBug(X,(Y+1));
				World.Creatures.add(DB);
				//reset breedtime
				TimeSinceLastBreeding = 0;
				
				return;
			}
			//check clearbelow...
			if(World.CheckClear(X, (Y-1)))
			{
				//JOptionPane.showMessageDialog(null,"Doodle just bred.");
				DoodleBug DB = new DoodleBug(X,(Y-1));
				World.Creatures.add(DB);
				//reset breedtime
				TimeSinceLastBreeding = 0;
				
				return;
			}
			//check clear right
			if(World.CheckClear((X+1), Y))
			{
				//JOptionPane.showMessageDialog(null,"Doodle just bred.");
				DoodleBug DB = new DoodleBug((X+1),Y);
				World.Creatures.add(DB);
				//reset breedtime
				TimeSinceLastBreeding = 0;
				
				return;
			}
			//check clear left
			if(World.CheckClear((X-1), Y))
			{
				//JOptionPane.showMessageDialog(null,"Doodle just bred.");
				DoodleBug DB = new DoodleBug((X-1),Y);
				World.Creatures.add(DB);
				//reset breedtime
				TimeSinceLastBreeding = 0;
				return;
			}
		}
	}
	
	
	//check if an ant is at location...
	public boolean AntAt(int x,int y)
	{
		for(int i = 0; i < World.Creatures.size(); i++)
		{
			if (World.Creatures.get(i).getX() == x && World.Creatures.get(i).getY() == y)
			{
				if (World.Creatures.get(i).Name == "Ant")
				{
					return true;
				}
			}
		}
		
		return false;
	}

}
