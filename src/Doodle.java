import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;

//where the main is kept :P
public class Doodle
{
	//statistics
	static int AntCount = 0;
	static int DoodleBugCount = 0;
	
	static boolean infiniteloop = true;
	
	//map object
	static World W = null;

	//holds main frame...
	private static JFrame JF = null;
	
	//label to display pop counts
	private static JLabel JL = null;
	
	//control frame
	private static JFrame C = new JFrame("Control Frame");
	
	//setup labels...
	public static JLabel [][] Labels = null;
	
	//create default border to give to labels...
	private static Border border = BorderFactory.createLineBorder(Color.BLACK,1);
	
	//load in icons...
	public static ImageIcon AntIcon = new ImageIcon("Ant.jpg");
	public static ImageIcon DoodleIcon = new ImageIcon("doodle.jpg");
	public static ImageIcon BlankIcon = new ImageIcon("blank_button.gif");
	
	public static void main(String args[])
	{
		
		//initialize world with 5 doodlebugs and 100 ants
		//setup size of world
		int WorldSize = 40; //50*50 = 2500 to start...
		
		//create JFrame...
		CreateJFrame(WorldSize);
		
			//size, doodlebugs, ants
			W = new World(WorldSize,5,100);
	
			while(infiniteloop == true)
			{
				W.Update();
				UpdateFrame();
			
				//wait so stuff can update or whatever
				try 
				{
					Thread.sleep(100);// 1/10 a second
				}
				catch(InterruptedException ex) 
				{
					Thread.currentThread().interrupt();
				}
			}
		
		
	}
	
	public static void SetupLabels(int WorldSize)
	{
		Labels = new JLabel[WorldSize][WorldSize];
		
		//For each label setup defaults
		for(int y = 0; y < Labels.length; y++)
		{
			for(int x = 0; x < Labels.length; x++)
			{
				//create label object and place in array
				Labels[y][x] = new JLabel();
				//set starting text
				Labels[y][x].setText("");
				//set border
				Labels[y][x].setBorder(border);
				//set horizontal alignments
				Labels[y][x].setHorizontalAlignment(SwingConstants.CENTER);
				//set vertical alignment
				Labels[y][x].setVerticalAlignment(SwingConstants.CENTER);
			
				//blank icon by default...
				Labels[y][x].setIcon(BlankIcon);
			
				//finally add completed label to form 1
				JF.add(Labels[y][x]);
			
			}
		}
		
	}
	
	public static void CreateJFrame(int WorldSize)
	{
		JF = new JFrame("Simulation");
		
		//assign layouts to jframes to prevent pandemonium 
		//3 rows, 3 columns, 5 pixels between them, and one pixel to rule them all
		JF.getContentPane().setLayout(new GridLayout(WorldSize,WorldSize,0,0));
		
		//Give frame dimensions
		JF.setSize(600, 600);
		
		//center the frame
		JF.setLocationRelativeTo(null);
		
		//set program termination on labels
		JF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		//setup labels and add to form...
		SetupLabels(WorldSize);
		
		//set frame visible
		JF.setVisible(true);
		
		//frame 2!
		
		//3 rows, 3 columns, 5 pixels between them, and one pixel to rule them all
		C.getContentPane().setLayout(new GridLayout(4,1,0,0));
				
		//Give frame dimensions
		C.setSize(400, 200);
				
		//center the frame
		//C.setLocationRelativeTo(null);
				
		//set program termination on labels
		C.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					
		JButton JB = new JButton("Next Step");
		
		JB.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				W.Update();
				UpdateFrame();			
			}
			
		});
		
		JButton JB1 = new JButton("Next 10 Steps");
		
		JB1.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				for(int i = 0; i < 10; i++)
				{
					W.Update();
					UpdateFrame();
					//repaint the wall or something idk hehe
					JF.repaint();
				}
			}
			
		});
		
		JButton JB2 = new JButton("Stop Auto Sim");
		
		JB2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				
			    infiniteloop = false;
				//repaint the wall or something idk hehe
				JF.repaint();
			}
			
		});
		
		
		JL = new JLabel("Counts To Display Here.");
		
		//add label
		C.add(JL);
		
		//add button to c frame
		C.add(JB);
		

		//add another button
		C.add(JB1);
		
		//add another button
		C.add(JB2);
		
		
		//set frame visible
		C.setVisible(true);
	}
	
	public static void UpdateFrame()
	{
		//recount everything
		DoodleBugCount = 0;
		AntCount = 0;
		
		//For each label setup defaults
		for(int y = 0; y < Labels.length; y++)
		{
			for(int x = 0; x < Labels.length; x++)
			{
				if(World.WorldMap[y][x] == '-')
				{
					Labels[y][x].setIcon(BlankIcon);
				}
				else if (World.WorldMap[y][x] == 'D')
				{
					Labels[y][x].setIcon(DoodleIcon);
					//counting...
					DoodleBugCount++;
				}
				else if (World.WorldMap[y][x] == 'A')
				{
					Labels[y][x].setIcon(AntIcon);
					//counting...
					AntCount++;
				}
			}
		}
		
		JL.setText("Ant Pop: "+AntCount+" Doodle Count: "+DoodleBugCount);
	}
	
	
}
